/*
 * MemProcess.cpp
 *
 *  Created on: Feb 6, 2015
 *      Author: xiangyuwang
 */

#include "MemProcess.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdlib.h>

#define EXTENSION_OF_USERPROGRAM_FILE ".txt"
#define SYSTEM_STACK_START    1000
#define SYSTEM_STACK_END		1999
#define USER_STACK_START		0
#define USER_STACK_END			999

using namespace std;

MemProcess::MemProcess(pid_t pid, int *pPipe_in, int *pPipe_out) {
	memset(memoryArray, 0, MEMORY_LENGTH*sizeof(int));
	Mempid = pid;
	pMempipe_in = pPipe_in;  //for read
	pMempipe_out = pPipe_out; //for write
}

int MemProcess::Init(const char *pFileName)
{
	int len = strlen(pFileName);
	if(len == 0)
		return 1; //fail
	//cout<<"The user program file is "<<pFileName<<endl;
	if(strcmp(&pFileName[len-strlen(EXTENSION_OF_USERPROGRAM_FILE)], EXTENSION_OF_USERPROGRAM_FILE))  //not ".txt", returen fail;
	{
		cout<<"user program file is not a txt file: "<<pFileName<<endl;
		return 1;
	}

	ifstream ifStream(pFileName);
	string lineStr;
	int memAddr = 0;
	int ins;
	bool bladdr = false;
	while(getline(ifStream, lineStr))//till the end of the user program file
	{
		getIns(lineStr, ins, bladdr);
		if(-1 != ins)
		{
			if(true == bladdr)
			{
				memAddr = ins;
			}
			else
			{
				//cout<<"store user instruction: address: "<< memAddr<<", instruction: "<<ins<<endl;
				memoryArray[memAddr] = ins;
				memAddr++;
			}
		}

	}
	ifStream.close();
	//cout<<"MemProcess: memory initiation is done!"<<endl;
	//pipeWrite(NULL, (char*)MEM_INIT_OK);
	return 0;
}
void MemProcess::start()
{
	char buffer[MAX_MESSAGE_LEN +1] = {'\0'};
	while(1)
	{
		memset(buffer, '\0', (MAX_MESSAGE_LEN +1)* sizeof(char));
		read(pMempipe_in[0], buffer, MAX_MESSAGE_LEN * sizeof(char));
		if(strlen(buffer) == 0)
		{
			continue;
		}

		//cout<<"MemProcess start: the message is "<<buffer<<endl;
		MEMrec_message parsed;
		if(-1 == parseMessage(buffer, parsed))
			continue;
		execute(parsed);

	}
	return;
}
void MemProcess::getIns(const string line, int &ins, bool &bladdr)
{
	bool begin = false; //false: the required instruction has not begun yet
	bool add_begin = false;  // true: the data of this line is address
	string instruction;
	const char *pIter = line.c_str();
	int len = line.size();
	//cout<<"len = "<<len<<", line text: "<< line <<endl;

	for(int i = 0; i<len; i++)
	{
		if(pIter[i] == ' ')
		{
			if(begin != false)
				break;
		}
		else if(pIter[i] == '.')
		{
			if(begin == false)
			{
				add_begin = true;
			}
			else
				break;
		}

		else if((pIter[i] >= '0') && (pIter[i] <= '9'))
		{
			begin = true;
			instruction+=pIter[i];
		}
		else // such as '/' no matter it's the first or not
		{
			break;
		}
	}

	if(true == begin)
	{
		ins = atoi(instruction.c_str());
		if(add_begin == true)  //the address to store the instructions
		{
			bladdr = true;
			//cout<<"address: "<<instruction<<endl;
		}
		else
		{
			//cout<<"instruction: "<<instruction<<endl;
			bladdr = false;
			return;
		}
	}
	else
	{
		ins = -1;
	}
}
/*
 * pipeWrite
 * recMes: taken from the message sent from CPU,such as: "fetch:address", "pop:address:x" "pop:address:pc"
 */
void MemProcess::pipeWrite(const char *newData)
{
	if((NULL != newData) &&(strlen(newData) != 0))
	{
		//cout<<"MemProcess pipeWrite: MESSAGE: "<<newData<<endl;
		write(pMempipe_out[1], newData, strlen(newData) *sizeof(char));
	}
}
int MemProcess::parseMessage(char *toParse, MEMrec_message &parsed)
{
	char *p = strtok(toParse, IPC_MESSAGE_SEPARATOR);

	parsed.action = (IPC_TYPE)atoi(p);

	p = strtok(NULL, IPC_MESSAGE_SEPARATOR);
	if(p!= NULL)
		parsed.address = atoi(p);

	if(parsed.action == ENUM_IPC_WRITE)
	{
		p = strtok(NULL, IPC_MESSAGE_SEPARATOR);
		if(p!= NULL)
			parsed.data = atoi(p);

	}

	return 0;
}
void MemProcess::toString(int i, char *temp)
{
	stringstream ss;
	ss<<i;
	ss>>temp;
}
void MemProcess::execute(MEMrec_message parsed)
{
	//cout<<"MemProcess: execute"<<endl;
	if(parsed.action == ENUM_IPC_READ)
	{
		stringstream ss;
		char data[MAX_ADDRESS_LEN + 1] = {'\0'};
		ss<<memoryArray[parsed.address];
		ss>>data;

		pipeWrite(data);
	}
	else if(parsed.action == ENUM_IPC_WRITE)
	{
		memoryArray[parsed.address] = parsed.data;

		pipeWrite(IPC_OK);
	}
}
MemProcess::~MemProcess() {
	// TODO Auto-generated destructor stub
}

