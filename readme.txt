
* Project description:
The project will simulate a simple computer system consisting of a CPU and Memory.
The CPU and Memory will be simulated by separate processes that communicate.
* Objectives
1)	Learn how multiple processes can communicate and cooperate.
2)	Understand low-level concepts important to an operating system. 
a.	Processor interaction with main memory.
b.	Processor instruction behavior.
c.	Role of registers.
d.	Stack processing.
e.	Procedure calls.	f.	System calls.
g.	Interrupt handling.
h.	Memory protection.
i.	I/O.
* Design:
C++
use a Unix fork to create processes and a Unix pipe for communication

* Submission:
OSProject1.cpp: main() function, creates the pipe and child process as memory process, and  starts the memory process and CPU process.
CPUProcess.cpp: the class for CPU process,
MemProcess.cpp: the class for memory process

IPC.h    defines some macros for both memory and CPU process.
MemProcess.h the prototype of MemProcess
CPUProcess.h the prototype of CPUProcess
sample5.txt: the user program file created by myself. It’s an apple. Hope you can recognize ^-^