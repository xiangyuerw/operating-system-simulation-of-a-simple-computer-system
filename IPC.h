/*
 * IPC.h
 *
 *  Created on: Feb 20, 2015
 *      Author: xiangyuwang
 */

#ifndef INC_IPC_H_
#define INC_IPC_H_
#define MAX_MESSAGE_LEN 50
#define MAX_ADDRESS_LEN 4

#define PIPE_WRITE_SLEEP 1

#define TO_MEMORY_HEADER "TOMEM"
#define TO_CPU_HEADER "TOCPU"
#define IPC_OK "OK"
#define MEM_INIT_OK "MEMORY OK!"
#define IPC_ACTION_FETCH "fetch"
#define IPC_ACTION_STORE "store"
#define IPC_ACTION_POP  "pop"  //after pop, the space should be cleared
#define IPC_ACTION_PUSHES  "pushes"//for start system mode
#define IPC_ACTION_POPS  "pops"  //for restore user mode
#define NUMBER_TO_PUSH_POP 2 //SP, PC//6//SP,IR,PC,AC,X,Y
#define IPC_POP_PC "PC"
#define IPC_POP_IR "IR"
#define IPC_POP_SP "SP"
#define IPC_POP_AC "AC"
#define IPC_POP_X "X"
#define IPC_POP_Y "Y"
#define IPC_MESSAGE_SEPARATOR ":"
#define IPC_MESSAGE_PUSH_POP_SEPARATOR ","


enum IPC_TYPE{
	ENUM_IPC_READ,
	ENUM_IPC_WRITE
};
#endif /* INC_IPC_H_ */
