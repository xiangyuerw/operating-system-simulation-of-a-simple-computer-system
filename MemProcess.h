/*
 * MemProcess.h
 *
 *  Created on: Feb 6, 2015
 *      Author: xiangyuwang
 */
#include <sys/types.h> //for pid_t
#include <unistd.h>//for fork() and pipe()
#include <string>
#include "IPC.h"
using namespace std;
#ifndef MEMPROCESS_H_
#define MEMPROCESS_H_
#define MEMORY_LENGTH  2000
struct MEMrec_message{
	int address;
	int data;
	IPC_TYPE action;
};

class MemProcess {
private:
	int memoryArray[MEMORY_LENGTH];
	int readFile();
	int storeUserProgram();
	pid_t Mempid;
	int *pMempipe_in;
	int *pMempipe_out;
	string reply_head;

	void getIns(const string line, int &ins, bool &bladdr);
	void pipeWrite(const char *newData);
	int parseMessage(char *toParse, MEMrec_message &parsed);
	void execute(MEMrec_message parsed);
	void toString(int i, char *temp);
public:
	MemProcess(pid_t pid, int *pPipe_in, int *pPipe_out);
	int Init(const char *pFileName);
	void start();
	~MemProcess();
};

#endif /* MEMPROCESS_H_ */
