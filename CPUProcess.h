/*
 * CPUProcess.h
 *
 *  Created on: Feb 6, 2015
 *      Author: xiangyuwang
 */
#include "sys/types.h"
#include <unistd.h>//for fork() and pipe()
#include "IPC.h"

#ifndef CPUPROCESS_H_
#define CPUPROCESS_H_

enum CPU_STATE{
	ENUM_READ_INSTRUCTION,
	ENUM_READ_DATA,
	ENUM_READ_ADDRESS1,
	ENUM_READ_ADDRESS,
	ENUM_PUSH,
	ENUM_POP,
	ENUM_PUSH_SP,//FOR INTERRUPT
	ENUM_PUSH_PC,//FOR INTERRUPT
	ENUM_POP_PC,//FOR INTERRUPT
	ENUM_POP_SP //FOR INTERRUPT
};

class CPUProcess {
private:
	int AC, PC, SP, X, Y, IR;
	int inttimer;
	int ins_counter;   //Count how many instruction has been executed until inttime;
	pid_t CPUpid;
	int *pCPUpipe_in;  //The same as Mempipe
	int *pCPUpipe_out;
	bool blSys;
	bool blWaiting;  //WAITING FOR THE DATA OR ADDRESS NEEDED FOR THE EXECUTION OF THE INSTRUCTION
	bool blRet;  //CPU is returning from system mode. It should be false after completion.
	CPU_STATE cpuState;
	bool blTimer;
	int received;

	int execute(char *message);
	int getInstruction(char *message);
	void RestoreUserMode();
	void writePipe(IPC_TYPE type, int address, int data);
	void parseMessage(char *message, int &data);
	int executeforPOP();
	int executeforPOPs();
	int fetchNext(IPC_TYPE type, int address, int data);
	void checkTimer();

public:
	CPUProcess(pid_t pid, int time, int *pPipe_in, int *pPipe_out);
	void start();

	~CPUProcess();
};

#endif /* CPUPROCESS_H_ */
