/*
 * CPUProcess.cpp
 *
 *  Created on: Feb 6, 2015
 *      Author: xiangyuwang
 */
#include "CPUProcess.h"
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdlib.h>

using namespace std;

#define SYSTEM_CALL_ISR 1500
#define TIMER_ISR  1000
#define SYSTEM_STACK_START 1999
#define USER_STACK_START 999

CPUProcess::CPUProcess(pid_t pid, int timer, int *pPipe_in, int *pPipe_out) {

	AC=IR=PC=X=Y=0;
	SP = USER_STACK_START +1;
	CPUpid = pid;
	pCPUpipe_in = pPipe_in;
	pCPUpipe_out=pPipe_out;
	inttimer = timer;
	ins_counter = 0;
	blSys = false;  //if true, Cpu is in system mode, interrupt will be deactivated
	blWaiting = false; // if true, don't fetch instruction
	blRet = false;
	cpuState = ENUM_READ_INSTRUCTION;
	blTimer = false;
	received = 0;
}

void CPUProcess::start()
{
	char buffer[MAX_MESSAGE_LEN + 1] = "\0";
	fetchNext(ENUM_IPC_READ, PC, 0);
	while(1)
	{
		memset(buffer, '\0', (MAX_MESSAGE_LEN +1)*sizeof(char));
		read(pCPUpipe_in[0], buffer, MAX_MESSAGE_LEN*sizeof(char));
		if(strlen(buffer) == 0)
			continue;

		blWaiting = false;

		int ret = execute(buffer);
		if(-1 == ret)
		{
			cout<<"CPUProcess: execute failed!"<<endl;
			break;
		}
		if(1 == ret)
		{
			cout<<"CPUProcess: execution completes!"<<endl;
			break;
		}
	}

}
int CPUProcess::execute(char *message)
{
	int data;
	parseMessage(message, data);
	if(blTimer == true)
	{
		if(cpuState == ENUM_PUSH_SP)
		{
			blWaiting = true;
			cpuState = ENUM_PUSH_PC;
			return fetchNext(ENUM_IPC_WRITE, --SP, PC);
		}
		else if(cpuState == ENUM_PUSH_PC)
		{
			PC = TIMER_ISR;
			blTimer = false;
			blWaiting = false;
			blTimer = false;
			cpuState = ENUM_READ_INSTRUCTION;
			return fetchNext(ENUM_IPC_READ, PC, 0);
		}
	}
	else if(cpuState == ENUM_READ_INSTRUCTION)
	{
		IR =data;
	}

	switch(IR)
	{
	case 1:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}

		break;
	case 2:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_ADDRESS)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, data, 0);
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{

			cpuState = ENUM_READ_ADDRESS;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}

		break;
	case 3:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_ADDRESS)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, data, 0);
		}
		else if(cpuState == ENUM_READ_ADDRESS1) //There's address in it, not data
		{
			cpuState = ENUM_READ_ADDRESS;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, data, 0);
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_ADDRESS1;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 4:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_ADDRESS)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, data+X, 0);

		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_ADDRESS;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 5:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_ADDRESS)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, data+Y, 0);
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_ADDRESS;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 6:
		if(cpuState == ENUM_READ_DATA)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, SP+X, 0);
		}
		break;
	case 7:
		if(cpuState == ENUM_PUSH)
		{
			if(blSys == false)
				ins_counter ++;
			PC++;
		}
		else if(cpuState == ENUM_READ_DATA)
		{
			blWaiting = true;
			cpuState = ENUM_PUSH;
			return fetchNext(ENUM_IPC_WRITE, data, AC);

		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 8:
		srand((unsigned )time(0));
		AC = 1+random()%101;
		if(blSys == false)
			ins_counter ++;
		break;
	case 9:
		if(cpuState == ENUM_READ_DATA)
		{
			if(data == 1)
			{
				cout<<AC; //AC as int to the screen;
			}
			if(data == 2)
			{
				char temp = AC;

				cout<<temp;  //AC as char to the screen
			}
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 10:
		PC++;
		AC += X;
		if(blSys == false)
			ins_counter ++;
		break;
	case 11:
		PC++;
		AC += Y;
		if(blSys == false)
			ins_counter ++;
		break;
	case 12:
		PC++;
		AC -= X;
		if(blSys == false)
			ins_counter ++;
		break;
	case 13:
		PC++;
		AC -= Y;
		if(blSys == false)
			ins_counter ++;
		break;
	case 14:
		PC++;
		X = AC;
		if(blSys == false)
			ins_counter ++;
		break;
	case 15:
		PC++;
		AC = X;
		if(blSys == false)
			ins_counter ++;
		break;
	case 16:
		PC++;
		Y = AC;
		if(blSys == false)
			ins_counter ++;
		break;
	case 17:
		PC++;
		AC = Y;
		if(blSys == false)
			ins_counter ++;
		break;
	case 18:
		PC++;
		SP = AC;
		if(blSys == false)
			ins_counter ++;
		break;
	case 19:
		PC++;
		AC = SP;
		if(blSys == false)
			ins_counter ++;
		break;
	case 20:
		if(cpuState == ENUM_READ_DATA)
		{
			PC = data;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 21:
		if(cpuState == ENUM_READ_DATA)
		{
			if(AC == 0)
				PC = data;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			if(AC == 0)
			{
				cpuState = ENUM_READ_DATA;
				blWaiting = true;
				return fetchNext(ENUM_IPC_READ, ++PC, 0);
			}
			else
			{
				PC = PC+2;
				if(blSys == false)
					ins_counter ++;
			}

		}
		break;
	case 22:
		if(cpuState == ENUM_READ_DATA)
		{
			if(AC != 0)
				PC = data;
			else
				PC++;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			if(AC != 0)
			{
				cpuState = ENUM_READ_DATA;
				blWaiting = true;
				return fetchNext(ENUM_IPC_READ, ++PC, 0);
			}
			else
			{
				if(blSys == false)
					ins_counter ++;
				PC+=2;
			}

		}
		break;
	case 23:
		if(cpuState == ENUM_PUSH)
		{
			if(blSys == false)
				ins_counter ++;
		}
		if(cpuState == ENUM_READ_DATA)
		{
			int tmp = ++PC;
			PC = data;
			blWaiting = true;
			cpuState = ENUM_PUSH;
			return fetchNext(ENUM_IPC_WRITE, --SP, tmp);
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_READ_DATA;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, ++PC, 0);
		}
		break;
	case 24:
		if(cpuState == ENUM_POP)
		{
			PC = data;
			if(blSys == false)
				ins_counter ++;
		}
		else if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_POP;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, SP++, 0);
		}
		break;
	case 25:
		X++;
		PC++;
		if(blSys == false)
			ins_counter ++;
		break;
	case 26:
		X--;
		PC++;
		if(blSys == false)
			ins_counter ++;
		break;
	case 27:
		if(cpuState == ENUM_READ_INSTRUCTION)
		{
			blWaiting = true;
			cpuState = ENUM_PUSH;
			return fetchNext(ENUM_IPC_WRITE, --SP, AC);
		}
		else if(cpuState == ENUM_PUSH)
		{
			PC++;
			if(blSys == false)
				ins_counter ++;
		}
		break;
	case 28:
		if(cpuState == ENUM_READ_INSTRUCTION)
		{
			cpuState = ENUM_POP;
			blWaiting = true;
			return fetchNext(ENUM_IPC_READ, SP++, 0);
		}
		else if(cpuState == ENUM_POP)
		{
			AC = data;
			PC++;
			if(blSys == false)
				ins_counter++;
		}
		break;
	case 29:    //system call
		if(cpuState == ENUM_READ_INSTRUCTION)
		{
			blSys = true;
			int tempSP = SP;
			SP = SYSTEM_STACK_START + 1;
			blWaiting = true;
			cpuState = ENUM_PUSH_SP;
			PC++;
			return fetchNext(ENUM_IPC_WRITE, --SP, tempSP);
		}
		else if(cpuState == ENUM_PUSH_SP)
		{
			blWaiting = true;
			cpuState = ENUM_PUSH_PC;
			return fetchNext(ENUM_IPC_WRITE, --SP, PC);
		}
		else if(cpuState == ENUM_PUSH_PC)
		{
			PC = SYSTEM_CALL_ISR;

		}
		break;
	case 30:  //iRet
		if(cpuState == ENUM_READ_INSTRUCTION)
		{
			blWaiting = true;
			blRet = true;
			cpuState = ENUM_POP_PC;
			return fetchNext(ENUM_IPC_READ, SP++, 0);
			RestoreUserMode();
		}
		else if(cpuState == ENUM_POP_SP)
		{
			SP = data;
			blRet = false;
			blSys = false;
		}
		else if(cpuState == ENUM_POP_PC)
		{
			blWaiting = true;
			cpuState = ENUM_POP_SP;
			PC = data;
			return fetchNext(ENUM_IPC_READ, SP++, 0);
		}
		break;
	case 50:  //end of the user program
		return 1;
	default:
		break;

	}
	checkTimer();
	if((blWaiting == false) && (blRet == false))
	{
		cpuState = ENUM_READ_INSTRUCTION;
		return fetchNext(ENUM_IPC_READ, PC, 0);
	}
	return 0;
}
int CPUProcess::fetchNext(IPC_TYPE type, int address, int data)
{
	if((blSys == false) && (address > USER_STACK_START))
	{
		cout<<"Memory violation: in the user program, trying to fetch data in " <<PC<<endl;
		return -1;
	}

	writePipe(type, address, data);
	return 0;
}

void CPUProcess::checkTimer()
{
	if((ins_counter == inttimer) && blWaiting != true)//interrupt!
	{
		//cout<<"timer! StartSystemMode SP = "<<SP<<endl;
		blTimer = true;
		blSys = true;
		cpuState = ENUM_PUSH_SP;
		int tempSP = SP;
		SP = SYSTEM_STACK_START+ 1;
		fetchNext(ENUM_IPC_WRITE, --SP, tempSP);
		ins_counter = 0;
		blWaiting = true;
	}
}
void CPUProcess::parseMessage(char *message, int &data)
{
	if(!strcmp(message, IPC_OK))
	{

	}
	else
	{
		data = atoi(message);
	}
}

int CPUProcess::getInstruction(char *message)
{
	int instruction = 0;
	string myMes(message);
	//

	return instruction;
}

void CPUProcess::writePipe(IPC_TYPE type, int address, int data)
{
	string message;
	char buffer[MAX_ADDRESS_LEN + 1] = "\0";
	stringstream ss;
	ss<<int(type);
	ss>>buffer;
	message+=buffer;
	message+=IPC_MESSAGE_SEPARATOR;

	memset(buffer, (MAX_ADDRESS_LEN+1)*sizeof(char), '\0');
	stringstream ss1;
	ss1<<address;
	ss1>>buffer;
	message+=buffer;

	if(type == ENUM_IPC_WRITE)
	{
		stringstream ss;
		message+=IPC_MESSAGE_SEPARATOR;
		memset(buffer, (MAX_ADDRESS_LEN+1)*sizeof(char), '\0');
		ss<<data;
		ss>>buffer;
		message+=buffer;
	}
	//cout<<"CPUProcess pipeWrite: "<<message<<endl;
	write(pCPUpipe_out[1], message.c_str(), message.size());

}

CPUProcess::~CPUProcess() {
	// TODO Auto-generated destructor stub
}

