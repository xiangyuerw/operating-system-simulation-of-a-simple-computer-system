//============================================================================
// Name        : OSProject1.cpp
// Author      : Xiangyu Wang
// Version     :
// Copyright   : This is created by Xiangyu Wang
// Description :  in C++, Ansi-style
//============================================================================

#include <iostream>
//#include "stdio.h"
#include "MemProcess.h"
#include "CPUProcess.h"
#include <cstring>
#include <stdlib.h>

using namespace std;

int main(int argc, char * argv[]) {
	cout << "Operating System Project1 by Xiangyu Wang" << endl;
	int pfds1[2];//for CPU Process read, Memory Process write
	int pfds2[2]; //for CPU Process write, Memory process read

	char *pFileName = NULL;
	char *pTime = NULL;
	int time = 10;
	MemProcess *pMemProcess = NULL;
	CPUProcess *pCPUProcess = NULL;
	if((argc > 1) && (argc < 4))
	{
		int i = 1;
		while(argv[i])
		{
			if(i == 1)
			{
				pFileName = (char *)malloc((strlen(argv[i]) + 1)*sizeof(char));
				strcpy(pFileName, argv[i]);
			}
			else if(i == 2)
			{
				pTime = (char*) malloc((strlen(argv[i]) + 1)*sizeof(char));
				strcpy(pTime, argv[i]);
				time = atoi(pTime);
				free(pTime);
				//cout<<"timer: "<<time<<endl;
			}
			i++;
		}
	}
	else
	{
		//no parameter in the command line, then find on in the same directory???
		cout<<"No process is created and exit!"<<endl;
		exit(0);
	}
	int ret = pipe(pfds1);
	if (ret == -1)
	      exit(1);
	ret = pipe(pfds2);
	if(ret == -1)
		exit(1);
	switch (fork())
	{
	 case -1:
		 cout<<"Create process unsuccessfully!"<<endl;
		 exit(-1);
	case 0: //Child: MemProcess
		//cout<<"MemProcess start!"<< getpid()<<endl;
		pMemProcess = new MemProcess(getpid(), pfds2, pfds1);
		pMemProcess->Init(pFileName);
		pMemProcess->start();
		break;

	default: //Parent CPUProcess

		//cout<<"CPUProcess in the parent process!"<<getpid()<<endl;
		pCPUProcess = new CPUProcess(getpid(), time, pfds1, pfds2);
		pCPUProcess->start();

		break;

	}
	delete pCPUProcess;
	delete pMemProcess;
	//exit(0);  //This will kill the process
}
